use std::{env, path::PathBuf};

extern crate bindgen;

fn main() {
    println!("cargo:rerun-if-changed=wrapper.h");

    let home = env::var("HOME").unwrap();
    let toolchaindir = home.clone() + "/.roborio/arm-frc2022-linux-gnueabi";
    let cppver = "7.3.0";

    let bindings = bindgen::Builder::default()
        .header("wrapper.hpp")
        .clang_arg("-xc++")
        .clang_arg("-std=c++20")
        .clang_arg("--sysroot=".to_owned() + &toolchaindir)
        .clang_arg(
            "-isystem".to_owned()
                + &toolchaindir
                + "/usr/include/c++/"
                + cppver
                + "/arm-frc2022-linux-gnueabi/",
        )
        .clang_arg("-isystem".to_owned() + &toolchaindir + "/usr/include/c++/" + cppver)
        .clang_arg("-isystem".to_owned() + &toolchaindir + "/usr/include/")
        .clang_arg("-Iallwpilib/hal/src/main/native/include")
        .clang_arg("-Iallwpilib/ntcore/src/main/native/include")
        .clang_arg("-Iallwpilib/wpilibc/src/main/native/include")
        .clang_arg("-Iallwpilib/wpimath/src/main/native/include")
        .clang_arg("-Iallwpilib/wpiutil/src/main/native/include")
        .clang_arg("-Iallwpilib/wpiutil/src/main/native/fmtlib/include")
        .opaque_type("std::thread.*")
        .opaque_type("fmt.*")
        .allowlist_type("frc.*")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings!");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
