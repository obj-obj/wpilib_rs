use crate::*;

pub type AddressableLEDData = frc_AddressableLED_LEDData;
impl AddressableLEDData {
    pub fn new(r: u8, g: u8, b: u8) -> AddressableLEDData {
        AddressableLEDData {
            _base: HAL_AddressableLEDData {
                r,
                g,
                b,
                padding: 0,
            },
        }
    }

    pub fn set_rgb(&mut self, r: u8, g: u8, b: u8) {
        self._base.r = r;
        self._base.g = g;
        self._base.b = b;
    }

    pub fn set_hsv(&mut self, h: i32, s: i32, v: i32) {
        unsafe {
            self.SetHSV(h, s, v);
        }
    }

    // TODO SetLED fn
}

pub type AddressableLED = frc_AddressableLED;
impl AddressableLED {
    pub fn from(&mut self, port: i32) -> Self {
        unsafe {
            return AddressableLED::new(port);
        }
    }

    pub fn set_length(&mut self, length: i32) {
        unsafe {
            self.SetLength(length);
        }
    }

    pub fn set_data(&mut self, data: u8) {
        unsafe {
            self.SetData(data);
        }
    }

    pub fn set_bit_timing(
        &mut self,
        low0: units_time_nanosecond_t,
        high0: units_time_nanosecond_t,
        low1: units_time_nanosecond_t,
        high1: units_time_nanosecond_t,
    ) {
        unsafe {
            self.SetBitTiming(low0, high0, low1, high1);
        }
    }

    pub fn set_sync_time(&mut self, sync_time: units_time_nanosecond_t) {
        unsafe {
            self.SetSyncTime(sync_time);
        }
    }

    pub fn start(&mut self) {
        unsafe {
            self.Start();
        }
    }

    pub fn stop(&mut self) {
        unsafe {
            self.Stop();
        }
    }
}
