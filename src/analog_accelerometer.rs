use crate::*;

pub type AnalogAccelerometer = frc_AnalogAccelerometer;
impl AnalogAccelerometer {
    pub fn from(channel: i32) -> AnalogAccelerometer {
        unsafe {
            return AnalogAccelerometer::new(channel);
        }
    }

    pub fn from_input(channel: &mut AnalogInput) -> AnalogAccelerometer {
        unsafe {
            return AnalogAccelerometer::new1(channel);
        }
    }

    pub fn get_acceleration(&mut self) -> f64 {
        unsafe {
            return self.GetAcceleration();
        }
    }

    pub fn set_sensitivity(&mut self, sensitivity: f64) {
        unsafe {
            self.SetSensitivity(sensitivity);
        }
    }

    pub fn set_zero(&mut self, zero: f64) {
        unsafe {
            self.SetZero(zero);
        }
    }
}
