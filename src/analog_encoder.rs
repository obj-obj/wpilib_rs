use crate::*;

pub type AnalogEncoder = frc_AnalogEncoder;
impl AnalogEncoder {
    pub fn from(analog_input: i32) -> Self {
        unsafe {
            return AnalogEncoder::new(analog_input);
        }
    }

    pub fn from_input(analog_input: &mut AnalogInput) -> Self {
        unsafe {
            return AnalogEncoder::new1(analog_input);
        }
    }

    pub fn reset(&mut self) {
        unsafe {
            self.Reset();
        }
    }

    pub fn get(&self) -> units_angle_turn_t {
        unsafe {
            return self.Get();
        }
    }

    pub fn get_position_offset(&self) -> f64 {
        unsafe {
            return self.GetPositionOffset();
        }
    }

    pub fn set_distance_per_rotation(&mut self, distance_per_rotation: f64) {
        unsafe {
            self.SetDistancePerRotation(distance_per_rotation);
        }
    }

    pub fn get_distance_per_rotation(&self) -> f64 {
        unsafe {
            return self.GetDistancePerRotation();
        }
    }

    pub fn get_distance(&self) -> f64 {
        unsafe {
            return self.GetDistance();
        }
    }

    pub fn get_channel(&self) -> i32 {
        unsafe {
            return self.GetChannel();
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_AnalogEncoder_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
