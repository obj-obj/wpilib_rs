use crate::*;

pub type AnalogGyro = frc_AnalogGyro;
impl AnalogGyro {
    pub fn from(channel: i32) -> Self {
        unsafe {
            return AnalogGyro::new(channel);
        }
    }

    pub fn from_input(analog_input: &mut AnalogInput) -> Self {
        unsafe {
            return AnalogGyro::new1(analog_input);
        }
    }

    pub fn from_i32_preset(channel: i32, center: i32, offset: f64) -> Self {
        unsafe {
            return AnalogGyro::new3(channel, center, offset);
        }
    }

    pub fn set_sensitivity(&mut self, volts_per_degree_per_second: f64) {
        unsafe {
            self.SetSensitivity(volts_per_degree_per_second);
        }
    }

    pub fn set_deadband(&mut self, volts: f64) {
        unsafe {
            self.SetDeadband(volts);
        }
    }

    pub fn init_gyro(&mut self) {
        unsafe {
            self.InitGyro();
        }
    }

    pub fn get_analog_input(&self) -> std_shared_ptr {
        unsafe {
            return self.GetAnalogInput();
        }
    }

    pub fn get_angle(&mut self) -> f64 {
        unsafe {
            return frc_AnalogGyro_GetAngle(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_rate(&mut self) -> f64 {
        unsafe {
            return frc_AnalogGyro_GetRate(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_center(&mut self) -> i32 {
        unsafe {
            return frc_AnalogGyro_GetCenter(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_offset(&mut self) -> f64 {
        unsafe {
            return frc_AnalogGyro_GetOffset(self as *mut _ as *mut c_void);
        }
    }

    pub fn reset(&mut self) {
        unsafe {
            frc_AnalogGyro_Reset(self as *mut _ as *mut c_void);
        }
    }

    pub fn calibrate(&mut self) {
        unsafe {
            frc_AnalogGyro_Calibrate(self as *mut _ as *mut c_void);
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_AnalogGyro_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
