use crate::*;
use std::os::raw::c_void;

pub type AnalogInput = frc_AnalogInput;
impl AnalogInput {
    pub fn from(channel: i32) -> Self {
        unsafe {
            return AnalogInput::new(channel);
        }
    }

    pub fn get_value(&self) -> i32 {
        unsafe {
            return self.GetValue();
        }
    }

    pub fn get_average_value(&self) -> i32 {
        unsafe {
            return self.GetAverageValue();
        }
    }

    pub fn get_voltage(&self) -> f64 {
        unsafe {
            return self.GetVoltage();
        }
    }

    pub fn get_average_voltage(&self) -> f64 {
        unsafe {
            return self.GetAverageVoltage();
        }
    }

    pub fn get_channel(&self) -> i32 {
        unsafe {
            return self.GetChannel();
        }
    }

    pub fn set_average_bits(&mut self, bits: i32) {
        unsafe {
            self.SetAverageBits(bits);
        }
    }

    pub fn get_average_bits(&self) -> i32 {
        unsafe {
            return self.GetAverageBits();
        }
    }

    pub fn set_oversample_bits(&mut self, bits: i32) {
        unsafe {
            self.SetOversampleBits(bits);
        }
    }

    pub fn get_oversample_bits(&self) -> i32 {
        unsafe {
            return self.GetOversampleBits();
        }
    }

    pub fn get_lsb_weight(&self) -> i32 {
        unsafe {
            return self.GetLSBWeight();
        }
    }

    pub fn get_offset(&self) -> i32 {
        unsafe {
            return self.GetOffset();
        }
    }

    pub fn is_accumulator_channel(&self) -> bool {
        unsafe {
            return self.IsAccumulatorChannel();
        }
    }

    pub fn init_accumulator(&mut self) {
        unsafe {
            self.InitAccumulator();
        }
    }

    pub fn set_accumulator_initial_value(&mut self, value: i64) {
        unsafe {
            self.SetAccumulatorInitialValue(value);
        }
    }

    pub fn reset_accumulator(&mut self) {
        unsafe {
            self.ResetAccumulator();
        }
    }

    pub fn set_accumulator_center(&mut self, center: i32) {
        unsafe {
            self.SetAccumulatorCenter(center);
        }
    }

    pub fn set_accumulator_deadband(&mut self, deadband: i32) {
        unsafe {
            self.SetAccumulatorDeadband(deadband);
        }
    }

    pub fn get_accumulator_value(&self) -> i64 {
        unsafe {
            return self.GetAccumulatorValue();
        }
    }

    pub fn get_accumulator_count(&self) -> i64 {
        unsafe {
            return self.GetAccumulatorCount();
        }
    }

    pub fn get_accumulator_output(&self, value: &mut i64, count: &mut i64) {
        unsafe {
            self.GetAccumulatorOutput(value, count);
        }
    }

    pub fn set_sample_rate(samples_per_second: f64) {
        unsafe {
            AnalogInput::SetSampleRate(samples_per_second);
        }
    }

    pub fn get_sample_rate() -> f64 {
        unsafe {
            return AnalogInput::GetSampleRate();
        }
    }

    pub fn set_sim_device(&mut self, device: HAL_SimDeviceHandle) {
        unsafe {
            self.SetSimDevice(device);
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_AnalogInput_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
