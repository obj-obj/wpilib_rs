use crate::*;

pub type AnalogPotentiometer = frc_AnalogPotentiometer;
impl AnalogPotentiometer {
    pub fn from(channel: i32, full_range: f64, offset: f64) -> Self {
        unsafe {
            return AnalogPotentiometer::new(channel, full_range, offset);
        }
    }

    pub fn from_input(input: &mut AnalogInput, full_range: f64, offset: f64) -> Self {
        unsafe {
            return AnalogPotentiometer::new1(input, full_range, offset);
        }
    }

    pub fn get(&self) -> f64 {
        unsafe {
            return self.Get();
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_AnalogPotentiometer_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
