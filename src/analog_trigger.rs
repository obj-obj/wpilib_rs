pub use crate::*;
use std::os::raw::c_void;

pub type AnalogTrigger = frc_AnalogTrigger;
impl AnalogTrigger {
    pub fn from(channel: i32) -> Self {
        unsafe {
            return AnalogTrigger::new(channel);
        }
    }

    pub fn from_input(input: &mut AnalogInput) -> Self {
        unsafe {
            return AnalogTrigger::new1(input);
        }
    }

    pub fn set_limits_voltage(&mut self, lower: f64, upper: f64) {
        unsafe {
            self.SetLimitsVoltage(lower, upper);
        }
    }

    pub fn set_limits_duty_cycle(&mut self, lower: f64, upper: f64) {
        unsafe {
            self.SetLimitsDutyCycle(lower, upper);
        }
    }

    pub fn set_limits_raw(&mut self, lower: i32, upper: i32) {
        unsafe {
            self.SetLimitsRaw(lower, upper);
        }
    }

    pub fn set_averaged(&mut self, use_averaged_value: bool) {
        unsafe {
            self.SetAveraged(use_averaged_value);
        }
    }

    pub fn set_filtered(&mut self, use_filtered_value: bool) {
        unsafe {
            self.SetFiltered(use_filtered_value);
        }
    }

    pub fn get_index(&self) -> i32 {
        unsafe {
            return self.GetIndex();
        }
    }

    pub fn get_in_window(&mut self) -> bool {
        unsafe {
            return self.GetInWindow();
        }
    }

    pub fn get_trigger_state(&mut self) -> bool {
        unsafe {
            return self.GetTriggerState();
        }
    }

    pub fn create_output(&self, type_: AnalogTriggerType) -> std_shared_ptr {
        unsafe {
            return self.CreateOutput(type_);
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_AnalogTrigger_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
