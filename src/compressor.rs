use crate::*;

pub type CompressorConfigType = frc_CompressorConfigType;
pub type Compressor = frc_Compressor;
impl Compressor {
    pub fn from(module: i32, module_type: PneumaticsModuleType) -> Self {
        unsafe {
            return Compressor::new(module, module_type);
        }
    }

    pub fn from_default(module_type: PneumaticsModuleType) -> Self {
        unsafe {
            return Compressor::new1(module_type);
        }
    }

    pub fn start(&mut self) {
        unsafe {
            self.Start();
        }
    }

    pub fn stop(&mut self) {
        unsafe {
            self.Stop();
        }
    }

    pub fn enabled(&self) -> bool {
        unsafe {
            return self.Enabled();
        }
    }

    pub fn get_pressure_switch_value(&self) -> bool {
        unsafe {
            return self.GetPressureSwitchValue();
        }
    }

    pub fn get_current(&self) -> units_current_ampere_t {
        unsafe {
            return self.GetCurrent();
        }
    }

    pub fn get_analog_voltage(&self) -> units_voltage_volt_t {
        unsafe {
            return self.GetAnalogVoltage();
        }
    }

    pub fn disable(&mut self) {
        unsafe {
            self.Disable();
        }
    }

    pub fn enable_digital(&mut self) {
        unsafe {
            self.EnableDigital();
        }
    }

    pub fn enable_analog(
        &mut self,
        min_analog_voltage: units_voltage_volt_t,
        max_analog_voltage: units_voltage_volt_t,
    ) {
        unsafe {
            self.EnableAnalog(min_analog_voltage, max_analog_voltage);
        }
    }

    pub fn enable_hybrid(
        &mut self,
        min_analog_voltage: units_voltage_volt_t,
        max_analog_voltage: units_voltage_volt_t,
    ) {
        unsafe {
            self.EnableHybrid(min_analog_voltage, max_analog_voltage);
        }
    }

    pub fn get_config_type(&self) -> CompressorConfigType {
        unsafe {
            return self.GetConfigType();
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_Compressor_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
