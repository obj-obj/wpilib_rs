use crate::*;

pub type DigitalOutput = frc_DigitalOutput;
impl DigitalOutput {
    pub fn from(channel: i32) -> Self {
        unsafe {
            return DigitalOutput::new(channel);
        }
    }

    pub fn set(&mut self, value: bool) {
        unsafe {
            self.Set(value);
        }
    }

    pub fn get(&self) -> bool {
        unsafe {
            return self.Get();
        }
    }

    pub fn pulse(&mut self, length: f64) {
        unsafe {
            self.Pulse(length);
        }
    }

    pub fn is_pulsing(&self) -> bool {
        unsafe {
            return self.IsPulsing();
        }
    }

    pub fn set_pwm_rate(&mut self, rate: f64) {
        unsafe {
            self.SetPWMRate(rate);
        }
    }

    pub fn enable_pwm(&mut self, initial_duty_cycle: f64) {
        unsafe {
            self.EnablePWM(initial_duty_cycle);
        }
    }

    pub fn disable_pwm(&mut self) {
        unsafe {
            self.DisablePWM();
        }
    }

    pub fn update_duty_cycle(&mut self, duty_cycle: f64) {
        unsafe {
            self.UpdateDutyCycle(duty_cycle);
        }
    }

    pub fn set_sim_device(&mut self, device: HAL_SimDeviceHandle) {
        unsafe {
            self.SetSimDevice(device);
        }
    }

    pub fn get_port_handle_for_routing(&mut self) -> HAL_Handle {
        unsafe {
            return frc_DigitalOutput_GetPortHandleForRouting(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_analog_trigger_type_for_routing(&mut self) -> AnalogTriggerType {
        unsafe {
            return frc_DigitalOutput_GetAnalogTriggerTypeForRouting(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_channel(&mut self) -> i32 {
        unsafe {
            return frc_DigitalOutput_GetChannel(self as *mut _ as *mut c_void);
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_DigitalOutput_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
