use crate::*;

pub type DutyCycle = frc_DutyCycle;
impl DutyCycle {
    pub fn from(source: &mut DigitalSource) -> Self {
        unsafe {
            return DutyCycle::new(source);
        }
    }

    pub fn get_frequency(&self) -> i32 {
        unsafe {
            return self.GetFrequency();
        }
    }

    pub fn get_output(&self) -> f64 {
        unsafe {
            return self.GetOutput();
        }
    }

    pub fn get_output_raw(&self) -> u32 {
        unsafe {
            return self.GetOutputRaw();
        }
    }

    pub fn get_output_scale_factor(&self) -> u32 {
        unsafe {
            return self.GetOutputScaleFactor();
        }
    }

    pub fn get_fpga_index(&self) -> i32 {
        unsafe {
            return self.GetFPGAIndex();
        }
    }

    pub fn get_source_channel(&self) -> i32 {
        unsafe {
            return self.GetSourceChannel();
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_DutyCycle_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
