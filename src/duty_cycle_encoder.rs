use crate::*;

pub type DutyCycleEncoder = frc_DutyCycleEncoder;
impl DutyCycleEncoder {
    pub fn from(channel: i32) -> Self {
        unsafe {
            return DutyCycleEncoder::new(channel);
        }
    }

    pub fn from_duty_cycle(duty_cycle: &mut DutyCycle) -> Self {
        unsafe {
            return DutyCycleEncoder::new1(duty_cycle);
        }
    }

    pub fn from_source(source: &mut DigitalSource) -> Self {
        unsafe {
            return DutyCycleEncoder::new4(source);
        }
    }

    pub fn get_frequency(&self) -> i32 {
        unsafe {
            return self.GetFrequency();
        }
    }

    pub fn is_connected(&self) -> bool {
        unsafe {
            return self.IsConnected();
        }
    }

    pub fn set_connected_frequency_threshold(&mut self, frequency: i32) {
        unsafe {
            self.SetConnectedFrequencyThreshold(frequency);
        }
    }

    pub fn reset(&mut self) {
        unsafe {
            self.Reset();
        }
    }

    pub fn get(&self) -> units_angle_turn_t {
        unsafe {
            return self.Get();
        }
    }

    pub fn set_duty_cycle_range(&mut self, min: f64, max: f64) {
        unsafe {
            self.SetDutyCycleRange(min, max);
        }
    }

    pub fn set_distance_per_rotation(&mut self, distance_per_rotation: f64) {
        unsafe {
            self.SetDistancePerRotation(distance_per_rotation);
        }
    }

    pub fn get_distance_per_rotation(&self) -> f64 {
        unsafe {
            return self.GetDistancePerRotation();
        }
    }

    pub fn get_fpga_index(&self) -> i32 {
        unsafe {
            return self.GetFPGAIndex();
        }
    }

    pub fn get_source_channel(&self) -> i32 {
        unsafe {
            return self.GetSourceChannel();
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_DutyCycleEncoder_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
