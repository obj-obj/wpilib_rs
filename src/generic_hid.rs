use crate::*;

pub type RumbleType = frc_GenericHID_RumbleType;
pub type HIDType = frc_GenericHID_HIDType;
pub type GenericHID = frc_GenericHID;
impl GenericHID {
    pub fn from(port: i32) -> Self {
        unsafe {
            return GenericHID::new(port);
        }
    }

    pub fn get_raw_button(&self, button: i32) -> bool {
        unsafe {
            return self.GetRawButton(button);
        }
    }

    pub fn get_raw_button_pressed(&mut self, button: i32) -> bool {
        unsafe {
            return self.GetRawButtonPressed(button);
        }
    }

    pub fn get_raw_button_released(&mut self, button: i32) -> bool {
        unsafe {
            return self.GetRawButtonReleased(button);
        }
    }

    pub fn get_raw_axis(&self, axis: i32) -> f64 {
        unsafe {
            return self.GetRawAxis(axis);
        }
    }

    pub fn get_pov(&self, pov: i32) -> i32 {
        unsafe {
            return self.GetPOV(pov);
        }
    }

    pub fn get_axis_count(&self) -> i32 {
        unsafe {
            return self.GetAxisCount();
        }
    }

    pub fn get_pov_count(&self) -> i32 {
        unsafe {
            return self.GetPOVCount();
        }
    }

    pub fn get_button_count(&self) -> i32 {
        unsafe {
            return self.GetButtonCount();
        }
    }

    pub fn is_connected(&self) -> bool {
        unsafe {
            return self.IsConnected();
        }
    }

    pub fn get_type(&self) -> HIDType {
        unsafe {
            return self.GetType();
        }
    }

    // TODO: Convert to Rust string instead
    pub fn get_name(&self) -> std_string {
        unsafe {
            return self.GetName();
        }
    }

    pub fn get_axis_type(&self, axis: i32) -> i32 {
        unsafe {
            return self.GetAxisType(axis);
        }
    }

    pub fn get_port(&self) -> i32 {
        unsafe {
            return self.GetPort();
        }
    }

    pub fn set_output(&mut self, output_number: i32, value: bool) {
        unsafe {
            self.SetOutput(output_number, value);
        }
    }

    pub fn set_outputs(&mut self, value: i32) {
        unsafe {
            self.SetOutputs(value);
        }
    }

    pub fn set_rumble(&mut self, type_: RumbleType, value: f64) {
        unsafe {
            self.SetRumble(type_, value);
        }
    }
}
