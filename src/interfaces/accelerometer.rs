use crate::*;

pub type AccelerometerRange = frc_Accelerometer_Range;
pub type Accelerometer = frc_Accelerometer;
