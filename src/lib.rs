#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

// So that it doesn't need to be typed in every file
pub use std::os::raw::c_void;

// Type aliases
pub type SendableBuilder = wpi_SendableBuilder;

// Modules
mod interfaces;
pub use interfaces::*;
mod motorcontrol;
pub use motorcontrol::*;

// Files
mod addressable_led;
pub use addressable_led::*;
mod analog_accelerometer;
pub use analog_accelerometer::*;
mod analog_encoder;
pub use analog_encoder::*;
mod analog_gyro;
pub use analog_gyro::*;
mod analog_input;
pub use analog_input::*;
mod analog_output;
pub use analog_output::*;
mod analog_potentiometer;
pub use analog_potentiometer::*;
mod analog_trigger;
pub use analog_trigger::*;
mod analog_trigger_output;
pub use analog_trigger_output::*;
mod analog_trigger_type;
pub use analog_trigger_type::*;
mod built_in_accelerometer;
pub use built_in_accelerometer::*;
mod can;
pub use can::*;
mod compressor;
pub use compressor::*;
mod counter;
pub use counter::*;
mod dma;
pub use dma::*;
mod ds_control_word;
pub use ds_control_word::*;
mod debouncer;
pub use debouncer::*;
mod digital_glitch_filter;
pub use digital_glitch_filter::*;
mod digital_input;
pub use digital_input::*;
mod digital_output;
pub use digital_output::*;
mod digital_source;
pub use digital_source::*;
mod double_solenoid;
pub use double_solenoid::*;
mod driver_station;
pub use driver_station::*;
mod duty_cycle;
pub use duty_cycle::*;
mod duty_cycle_encoder;
pub use duty_cycle_encoder::*;
mod encoder;
pub use encoder::*;
mod generic_hid;
pub use generic_hid::*;
mod iterative_robot_base;
pub use iterative_robot_base::*;
mod joystick;
pub use joystick::*;
mod motor_safety;
pub use motor_safety::*;
mod notifier;
pub use notifier::*;
mod ps4_controller;
pub use ps4_controller::*;
mod pwm;
pub use pwm::*;
mod pneumatic_hub;
pub use pneumatic_hub::*;
mod pneumatics_control_module;
pub use pneumatics_control_module::*;
mod pneumatics_module_type;
pub use pneumatics_module_type::*;
mod power_distribution;
pub use power_distribution::*;
mod preferences;
pub use preferences::*;
mod relay;
pub use relay::*;
mod resource;
pub use resource::*;
mod robot_controller;
pub use robot_controller::*;
mod solenoid;
pub use solenoid::*;
