use crate::*;

pub type PneumaticHub = frc_PneumaticHub;
pub type PneumaticHubVersion = frc_PneumaticHub_Version;
pub type PneumaticHubFaults = frc_PneumaticHub_Faults;
pub type PneumaticHubStickyFaults = frc_PneumaticHub_StickyFaults;
impl PneumaticHub {
    pub fn from() -> Self {
        unsafe {
            return PneumaticHub::new();
        }
    }

    pub fn from_module(module: i32) -> Self {
        unsafe {
            return PneumaticHub::new1(module);
        }
    }

    pub fn get_version(&self) -> PneumaticHubVersion {
        unsafe {
            return self.GetVersion();
        }
    }

    pub fn get_faults(&self) -> PneumaticHubFaults {
        unsafe {
            return self.GetFaults();
        }
    }

    pub fn get_sticky_faults(&self) -> PneumaticHubStickyFaults {
        unsafe {
            return self.GetStickyFaults();
        }
    }

    pub fn clear_sticky_faults(&mut self) {
        unsafe {
            self.ClearStickyFaults();
        }
    }

    pub fn get_input_voltage(&self) -> units_voltage_volt_t {
        unsafe {
            return self.GetInputVoltage();
        }
    }

    pub fn get_5v_regulated_voltage(&self) -> units_voltage_volt_t {
        unsafe {
            return self.Get5VRegulatedVoltage();
        }
    }

    pub fn get_solenoids_total_current(&self) -> units_current_ampere_t {
        unsafe {
            return self.GetSolenoidsTotalCurrent();
        }
    }

    pub fn get_solenoids_voltage(&self) -> units_voltage_volt_t {
        unsafe {
            return self.GetSolenoidsVoltage();
        }
    }

    pub fn get_compressor(&mut self) -> bool {
        unsafe {
            return frc_PneumaticHub_GetCompressor(self as *mut _ as *mut c_void);
        }
    }

    pub fn disable_compressor(&mut self) {
        unsafe {
            frc_PneumaticHub_DisableCompressor(self as *mut _ as *mut c_void);
        }
    }

    pub fn enable_compressor_digital(&mut self) {
        unsafe {
            frc_PneumaticHub_EnableCompressorDigital(self as *mut _ as *mut c_void);
        }
    }

    pub fn enable_compressor_analog(
        &mut self,
        min_analog_voltage: units_voltage_volt_t,
        max_analog_voltage: units_voltage_volt_t,
    ) {
        unsafe {
            frc_PneumaticHub_EnableCompressorAnalog(
                self as *mut _ as *mut c_void,
                min_analog_voltage,
                max_analog_voltage,
            );
        }
    }

    pub fn enable_compressor_hybrid(
        &mut self,
        min_analog_voltage: units_voltage_volt_t,
        max_analog_voltage: units_voltage_volt_t,
    ) {
        unsafe {
            frc_PneumaticHub_EnableCompressorHybrid(
                self as *mut _ as *mut c_void,
                min_analog_voltage,
                max_analog_voltage,
            );
        }
    }

    pub fn get_compressor_config_type(&mut self) -> CompressorConfigType {
        unsafe {
            return frc_PneumaticHub_GetCompressorConfigType(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_pressure_switch(&mut self) -> bool {
        unsafe {
            return frc_PneumaticHub_GetPressureSwitch(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_compressor_current(&mut self) -> units_current_ampere_t {
        unsafe {
            return frc_PneumaticHub_GetCompressorCurrent(self as *mut _ as *mut c_void);
        }
    }

    pub fn set_solenoids(&mut self, mask: i32, value: i32) {
        unsafe {
            frc_PneumaticHub_SetSolenoids(self as *mut _ as *mut c_void, mask, value);
        }
    }

    pub fn get_solenoids(&mut self) -> i32 {
        unsafe {
            return frc_PneumaticHub_GetSolenoids(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_module_number(&mut self) -> i32 {
        unsafe {
            return frc_PneumaticHub_GetModuleNumber(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_solenoid_disabled_list(&mut self) -> i32 {
        unsafe {
            return frc_PneumaticHub_GetSolenoidDisabledList(self as *mut _ as *mut c_void);
        }
    }

    pub fn fire_one_shot(&mut self, index: i32) {
        unsafe {
            frc_PneumaticHub_FireOneShot(self as *mut _ as *mut c_void, index);
        }
    }

    pub fn set_one_shot_duration(&mut self, index: i32, duration: units_time_second_t) {
        unsafe {
            frc_PneumaticHub_SetOneShotDuration(self as *mut _ as *mut c_void, index, duration);
        }
    }

    pub fn check_solenoid_channel(&mut self, index: i32) -> bool {
        unsafe {
            return frc_PneumaticHub_CheckSolenoidChannel(self as *mut _ as *mut c_void, index);
        }
    }

    pub fn check_and_reserve_solenoids(&mut self, mask: i32) -> i32 {
        unsafe {
            return frc_PneumaticHub_CheckAndReserveSolenoids(self as *mut _ as *mut c_void, mask);
        }
    }

    pub fn unreserve_solenoids(&mut self, mask: i32) {
        unsafe {
            frc_PneumaticHub_UnreserveSolenoids(self as *mut _ as *mut c_void, mask);
        }
    }

    pub fn reserve_compressor(&mut self) -> bool {
        unsafe {
            return frc_PneumaticHub_ReserveCompressor(self as *mut _ as *mut c_void);
        }
    }

    pub fn unreserve_compressor(&mut self) {
        unsafe {
            frc_PneumaticHub_UnreserveCompressor(self as *mut _ as *mut c_void);
        }
    }

    pub fn make_solenoid(&mut self, channel: i32) -> Solenoid {
        unsafe {
            return frc_PneumaticHub_MakeSolenoid(self as *mut _ as *mut c_void, channel);
        }
    }

    pub fn make_double_solenoid(
        &mut self,
        forward_channel: i32,
        reverse_channel: i32,
    ) -> DoubleSolenoid {
        unsafe {
            return frc_PneumaticHub_MakeDoubleSolenoid(
                self as *mut _ as *mut c_void,
                forward_channel,
                reverse_channel,
            );
        }
    }

    pub fn make_compressor(&mut self) -> Compressor {
        unsafe {
            return frc_PneumaticHub_MakeCompressor(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_analog_voltage(&mut self, channel: i32) -> units_voltage_volt_t {
        unsafe {
            return frc_PneumaticHub_GetAnalogVoltage(self as *mut _ as *mut c_void, channel);
        }
    }
}
