use crate::*;

pub type PneumaticsControlModule = frc_PneumaticsControlModule;
impl PneumaticsControlModule {
    pub fn from() -> Self {
        unsafe {
            return PneumaticsControlModule::new();
        }
    }

    pub fn from_module(module: i32) -> Self {
        unsafe {
            return PneumaticsControlModule::new1(module);
        }
    }

    pub fn get_compressor_current_too_high_fault(&self) -> bool {
        unsafe {
            return self.GetCompressorCurrentTooHighFault();
        }
    }

    pub fn get_compressor_current_too_high_sticky_fault(&self) -> bool {
        unsafe {
            return self.GetCompressorCurrentTooHighStickyFault();
        }
    }

    pub fn get_compressor_shorted_fault(&self) -> bool {
        unsafe {
            return self.GetCompressorShortedFault();
        }
    }

    pub fn get_compressor_shorted_sticky_fault(&self) -> bool {
        unsafe {
            return self.GetCompressorShortedStickyFault();
        }
    }

    pub fn get_compressor_not_connected_fault(&self) -> bool {
        unsafe {
            return self.GetCompressorNotConnectedFault();
        }
    }

    pub fn get_compressor_not_connected_sticky_fault(&self) -> bool {
        unsafe {
            return self.GetCompressorNotConnectedStickyFault();
        }
    }

    pub fn clear_all_sticky_faults(&mut self) {
        unsafe {
            self.ClearAllStickyFaults();
        }
    }

    pub fn get_compressor(&mut self) -> bool {
        unsafe {
            return frc_PneumaticsControlModule_GetCompressor(self as *mut _ as *mut c_void);
        }
    }

    pub fn disable_compressor(&mut self) {
        unsafe {
            frc_PneumaticsControlModule_DisableCompressor(self as *mut _ as *mut c_void);
        }
    }

    pub fn enable_compressor_digital(&mut self) {
        unsafe {
            frc_PneumaticsControlModule_EnableCompressorDigital(self as *mut _ as *mut c_void);
        }
    }

    pub fn enable_compressor_analog(
        &mut self,
        min_analog_voltage: units_voltage_volt_t,
        max_analog_voltage: units_voltage_volt_t,
    ) {
        unsafe {
            frc_PneumaticsControlModule_EnableCompressorAnalog(
                self as *mut _ as *mut c_void,
                min_analog_voltage,
                max_analog_voltage,
            );
        }
    }

    pub fn enable_compressor_hybrid(
        &mut self,
        min_analog_voltage: units_voltage_volt_t,
        max_analog_voltage: units_voltage_volt_t,
    ) {
        unsafe {
            frc_PneumaticsControlModule_EnableCompressorHybrid(
                self as *mut _ as *mut c_void,
                min_analog_voltage,
                max_analog_voltage,
            );
        }
    }

    pub fn get_compressor_config_type(&mut self) -> CompressorConfigType {
        unsafe {
            return frc_PneumaticsControlModule_GetCompressorConfigType(
                self as *mut _ as *mut c_void,
            );
        }
    }

    pub fn get_pressure_switch(&mut self) -> bool {
        unsafe {
            return frc_PneumaticsControlModule_GetPressureSwitch(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_compressor_current(&mut self) -> units_current_ampere_t {
        unsafe {
            return frc_PneumaticsControlModule_GetCompressorCurrent(self as *mut _ as *mut c_void);
        }
    }

    pub fn set_solenoids(&mut self, mask: i32, values: i32) {
        unsafe {
            frc_PneumaticsControlModule_SetSolenoids(self as *mut _ as *mut c_void, mask, values);
        }
    }

    pub fn get_module_number(&mut self) -> i32 {
        unsafe {
            return frc_PneumaticsControlModule_GetModuleNumber(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_solenoid_disabled_list(&mut self) -> i32 {
        unsafe {
            return frc_PneumaticsControlModule_GetSolenoidDisabledList(
                self as *mut _ as *mut c_void,
            );
        }
    }

    pub fn fire_one_shot(&mut self, index: i32) {
        unsafe {
            frc_PneumaticsControlModule_FireOneShot(self as *mut _ as *mut c_void, index);
        }
    }

    pub fn set_one_shot_duration(&mut self, index: i32, duration: units_time_second_t) {
        unsafe {
            frc_PneumaticsControlModule_SetOneShotDuration(
                self as *mut _ as *mut c_void,
                index,
                duration,
            );
        }
    }

    pub fn check_solenoid_channel(&mut self, channel: i32) -> bool {
        unsafe {
            return frc_PneumaticsControlModule_CheckSolenoidChannel(
                self as *mut _ as *mut c_void,
                channel,
            );
        }
    }

    pub fn check_and_reserve_solenoids(&mut self, mask: i32) -> i32 {
        unsafe {
            return frc_PneumaticsControlModule_CheckAndReserveSolenoids(
                self as *mut _ as *mut c_void,
                mask,
            );
        }
    }

    pub fn unreserve_solenoids(&mut self, mask: i32) {
        unsafe {
            frc_PneumaticsControlModule_UnreserveSolenoids(self as *mut _ as *mut c_void, mask);
        }
    }

    pub fn reserve_compressor(&mut self) -> bool {
        unsafe {
            return frc_PneumaticsControlModule_ReserveCompressor(self as *mut _ as *mut c_void);
        }
    }

    pub fn unreserve_compressor(&mut self) {
        unsafe {
            frc_PneumaticsControlModule_UnreserveCompressor(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_analog_voltage(&mut self, channel: i32) -> units_voltage_volt_t {
        unsafe {
            return frc_PneumaticsControlModule_GetAnalogVoltage(
                self as *mut _ as *mut c_void,
                channel,
            );
        }
    }

    pub fn make_solenoid(&mut self, channel: i32) -> Solenoid {
        unsafe {
            return frc_PneumaticsControlModule_MakeSolenoid(
                self as *mut _ as *mut c_void,
                channel,
            );
        }
    }

    pub fn make_double_solenoid(
        &mut self,
        forward_channel: i32,
        reverse_channel: i32,
    ) -> DoubleSolenoid {
        unsafe {
            return frc_PneumaticsControlModule_MakeDoubleSolenoid(
                self as *mut _ as *mut c_void,
                forward_channel,
                reverse_channel,
            );
        }
    }

    pub fn make_compressor(&mut self) -> Compressor {
        unsafe {
            return frc_PneumaticsControlModule_MakeCompressor(self as *mut _ as *mut c_void);
        }
    }
}
