use crate::*;

pub type PowerDistribution = frc_PowerDistribution;
pub type PowerDistributionFaults = frc_PowerDistribution_Faults;
pub type PowerDistributionModuleType = frc_PowerDistribution_ModuleType;
pub type PowerDistrubutionStickyFaults = frc_PowerDistribution_StickyFaults;
pub type PowerDistributionVersion = frc_PowerDistribution_Version;
impl PowerDistribution {
    pub fn from() -> Self {
        unsafe {
            return PowerDistribution::new();
        }
    }

    pub fn from_module(module: i32, module_type: PowerDistributionModuleType) -> Self {
        unsafe {
            return PowerDistribution::new1(module, module_type);
        }
    }

    pub fn get_voltage(&self) -> f64 {
        unsafe {
            return self.GetVoltage();
        }
    }

    pub fn get_temperature(&self) -> f64 {
        unsafe {
            return self.GetTemperature();
        }
    }

    pub fn get_current(&self, channel: i32) -> f64 {
        unsafe {
            return self.GetCurrent(channel);
        }
    }

    pub fn get_total_current(&self) -> f64 {
        unsafe {
            return self.GetTotalCurrent();
        }
    }

    pub fn get_total_power(&self) -> f64 {
        unsafe {
            return self.GetTotalPower();
        }
    }

    pub fn get_total_energy(&self) -> f64 {
        unsafe {
            return self.GetTotalEnergy();
        }
    }

    pub fn reset_total_energy(&mut self) {
        unsafe {
            self.ResetTotalEnergy();
        }
    }

    pub fn clear_sticky_faults(&mut self) {
        unsafe {
            self.ClearStickyFaults();
        }
    }

    pub fn get_module(&self) -> i32 {
        unsafe {
            return self.GetModule();
        }
    }

    pub fn get_switchable_channel(&self) -> bool {
        unsafe {
            return self.GetSwitchableChannel();
        }
    }

    pub fn set_switchable_channel(&mut self, enabled: bool) {
        unsafe {
            self.SetSwitchableChannel(enabled);
        }
    }

    pub fn get_version(&self) -> PowerDistributionVersion {
        unsafe {
            return self.GetVersion();
        }
    }

    pub fn get_faults(&self) -> PowerDistributionFaults {
        unsafe {
            return self.GetFaults();
        }
    }

    pub fn get_sticky_faults(&self) -> PowerDistrubutionStickyFaults {
        unsafe {
            return self.GetStickyFaults();
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_PowerDistribution_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
