use crate::*;

pub type PS4Controller = frc_PS4Controller;
impl PS4Controller {
    pub fn from(port: i32) -> Self {
        unsafe {
            return PS4Controller::new(port);
        }
    }

    pub fn get_left_x(&self) -> f64 {
        unsafe {
            return self.GetLeftX();
        }
    }

    pub fn get_right_x(&self) -> f64 {
        unsafe {
            return self.GetRightX();
        }
    }

    pub fn get_left_y(&self) -> f64 {
        unsafe {
            return self.GetLeftY();
        }
    }

    pub fn get_right_y(&self) -> f64 {
        unsafe {
            return self.GetRightY();
        }
    }

    pub fn get_l2_axis(&self) -> f64 {
        unsafe {
            return self.GetL2Axis();
        }
    }

    pub fn get_r2_axis(&self) -> f64 {
        unsafe {
            return self.GetR2Axis();
        }
    }

    pub fn get_square_button(&self) -> bool {
        unsafe {
            return self.GetSquareButton();
        }
    }

    pub fn get_square_button_pressed(&mut self) -> bool {
        unsafe {
            return self.GetSquareButtonPressed();
        }
    }

    pub fn get_square_button_released(&mut self) -> bool {
        unsafe {
            return self.GetSquareButtonReleased();
        }
    }

    pub fn get_cross_button(&self) -> bool {
        unsafe {
            return self.GetCrossButton();
        }
    }

    pub fn get_cross_button_pressed(&mut self) -> bool {
        unsafe {
            return self.GetCrossButtonPressed();
        }
    }

    pub fn get_cross_button_released(&mut self) -> bool {
        unsafe {
            return self.GetCrossButtonReleased();
        }
    }

    pub fn get_circle_button(&self) -> bool {
        unsafe {
            return self.GetCircleButton();
        }
    }

    pub fn get_circle_button_pressed(&mut self) -> bool {
        unsafe {
            return self.GetCircleButtonPressed();
        }
    }

    pub fn get_circle_button_released(&mut self) -> bool {
        unsafe {
            return self.GetCircleButtonReleased();
        }
    }

    pub fn get_triangle_button(&self) -> bool {
        unsafe {
            return self.GetTriangleButton();
        }
    }

    pub fn get_triangle_button_pressed(&mut self) -> bool {
        unsafe {
            return self.GetTriangleButtonPressed();
        }
    }

    pub fn get_triangle_button_released(&mut self) -> bool {
        unsafe {
            return self.GetTriangleButtonReleased();
        }
    }

    pub fn get_l1_button(&self) -> bool {
        unsafe {
            return self.GetL1Button();
        }
    }

    pub fn get_l1_button_pressed(&mut self) -> bool {
        unsafe {
            return self.GetL1ButtonPressed();
        }
    }

    pub fn get_l1_button_released(&mut self) -> bool {
        unsafe {
            return self.GetL1ButtonReleased();
        }
    }

    pub fn get_r1_button(&self) -> bool {
        unsafe {
            return self.GetR1Button();
        }
    }

    pub fn get_r1_button_pressed(&mut self) -> bool {
        unsafe {
            return self.GetR1ButtonPressed();
        }
    }

    pub fn get_r1_button_released(&mut self) -> bool {
        unsafe {
            return self.GetR1ButtonReleased();
        }
    }

    pub fn get_l2_button(&self) -> bool {
        unsafe {
            return self.GetL2Button();
        }
    }

    pub fn get_l2_button_pressed(&mut self) -> bool {
        unsafe {
            return self.GetL2ButtonPressed();
        }
    }

    pub fn get_l2_button_released(&mut self) -> bool {
        unsafe {
            return self.GetL2ButtonReleased();
        }
    }

    pub fn get_r2_button(&self) -> bool {
        unsafe {
            return self.GetR2Button();
        }
    }

    pub fn get_r2_button_pressed(&mut self) -> bool {
        unsafe {
            return self.GetR2ButtonPressed();
        }
    }

    pub fn get_r2_button_released(&mut self) -> bool {
        unsafe {
            return self.GetR2ButtonReleased();
        }
    }

    pub fn get_share_button(&self) -> bool {
        unsafe {
            return self.GetShareButton();
        }
    }

    pub fn get_share_button_pressed(&mut self) -> bool {
        unsafe {
            return self.GetShareButtonPressed();
        }
    }

    pub fn get_share_button_released(&mut self) -> bool {
        unsafe {
            return self.GetShareButtonReleased();
        }
    }

    pub fn get_options_button(&self) -> bool {
        unsafe {
            return self.GetOptionsButton();
        }
    }

    pub fn get_options_button_pressed(&mut self) -> bool {
        unsafe {
            return self.GetOptionsButtonPressed();
        }
    }

    pub fn get_options_button_released(&mut self) -> bool {
        unsafe {
            return self.GetOptionsButtonReleased();
        }
    }

    pub fn get_l3_button(&self) -> bool {
        unsafe {
            return self.GetL3Button();
        }
    }

    pub fn get_l3_button_pressed(&mut self) -> bool {
        unsafe {
            return self.GetL3ButtonPressed();
        }
    }

    pub fn get_l3_button_released(&mut self) -> bool {
        unsafe {
            return self.GetL3ButtonReleased();
        }
    }

    pub fn get_r3_button(&self) -> bool {
        unsafe {
            return self.GetR3Button();
        }
    }

    pub fn get_r3_button_pressed(&mut self) -> bool {
        unsafe {
            return self.GetR3ButtonPressed();
        }
    }

    pub fn get_r3_button_released(&mut self) -> bool {
        unsafe {
            return self.GetR3ButtonReleased();
        }
    }

    pub fn get_ps_button(&self) -> bool {
        unsafe {
            return self.GetPSButton();
        }
    }

    pub fn get_ps_button_pressed(&mut self) -> bool {
        unsafe {
            return self.GetPSButtonPressed();
        }
    }

    pub fn get_ps_button_released(&mut self) -> bool {
        unsafe {
            return self.GetPSButtonReleased();
        }
    }

    pub fn get_touchpad(&self) -> bool {
        unsafe {
            return self.GetTouchpad();
        }
    }

    pub fn get_touchpad_pressed(&mut self) -> bool {
        unsafe {
            return self.GetTouchpadPressed();
        }
    }

    pub fn get_touchpad_released(&mut self) -> bool {
        unsafe {
            return self.GetTouchpadReleased();
        }
    }
}
