use crate::*;

pub type Relay = frc_Relay;
pub type RelayDirection = frc_Relay_Direction;
pub type RelayValue = frc_Relay_Value;
impl Relay {
    pub fn from(channel: i32, direction: RelayDirection) -> Self {
        unsafe {
            return Relay::new(channel, direction);
        }
    }

    pub fn set(&mut self, value: RelayValue) {
        unsafe {
            self.Set(value);
        }
    }

    pub fn get(&self) -> RelayValue {
        unsafe {
            return self.Get();
        }
    }

    pub fn get_channel(&self) -> i32 {
        unsafe {
            return self.GetChannel();
        }
    }

    pub fn stop_motor(&mut self) {
        unsafe {
            frc_Relay_StopMotor(self as *mut _ as *mut c_void);
        }
    }

    // TODO: Convert to Rust string instead
    pub fn get_description(&mut self) -> std_string {
        unsafe {
            return frc_Relay_GetDescription(self as *mut _ as *mut c_void);
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_Relay_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
