use crate::*;

pub type Resource = frc_Resource;
impl Resource {
    pub fn from(size: u32) -> Self {
        unsafe {
            return Resource::new(size);
        }
    }

    // TODO: This class uses C++ types (std::string & std::unique_ptr)
    // Converting to/from these types to rust types isn't implemented yet
}
